create table if not exists clients
(
    id serial primary key,
    address   varchar(255),
    email     varchar(255) not null,
    firstname varchar(100),
    lastname  varchar(100),
    mobile    varchar(15),
    notes     text,
    status    boolean      not null
);

create table if not exists products
(
    id serial primary key,
    category            varchar(255)     not null,
    denomination        varchar(100)     not null,
    gear_box            varchar(255)     not null,
    number_of_beds      integer          not null,
    picture             varchar(255)     not null,
    price_ttc           double precision not null,
    year_of_manufacture integer          not null
);

create table if not exists orders
(
    id serial primary key,
    order_date     varchar(255) not null,
    order_state    varchar(100) not null,
    payment_method varchar(100) not null,
    client_id      integer      not null
        constraint fk_orders_client_id
            references clients,
    product_id     integer
        constraint fk_orders_product_id
            references products
);

create table if not exists users
(
    id serial primary key,
    email           varchar(255),
    employee_number integer,
    firstname       varchar(30),
    grants          varchar(255),
    job             varchar(255),
    lastname        varchar(30),
    password        varchar(255),
    username        varchar(30)
);

--############################################################
-- Jeu de données
insert into users (email, employee_number, firstname, grants, job, lastname, password, username)
values ('marc.suro@campingcar.com', 10110, 'Marc', 'ADMIN', 'Directeur', 'Suro',
        '$2a$12$pHMNlhBLA6YFU4WScanAIOU5X7aV4GTJxI7/WF.VAcJejKXKhAi4m', 'marc');


insert into users (email, employee_number, firstname, grants, job, lastname, password, username)
values ('bruno.lefont@campingcar.com', 10213, 'Bruno', 'USER', 'Comptable', 'lafont',
        '$2a$10$lkP1I5.vblqWVG.AEsIIPOXXHcKbBTSPootXIYvCennE67YDVKa4i', 'bruno');
insert into clients (address, email, firstname, lastname, mobile, notes, status)
values ('24 avenue Louis Bertaud', 'morane.bob@gmail.com', 'Bob', 'Morane', '06000000', 'test', false);
insert into clients (address, email, firstname, lastname, mobile, notes, status)
values ('Gotham', 'wayne.bruce@gmail.com', 'Bruce', 'Wayne', '0666666666', null, true);
insert into clients (address, email, firstname, lastname, mobile, notes, status)
values ('3 boulevard des tournesols', 'regis.dasso@gmail.com', 'Regis', 'Dasso', '655698895', null, true);
insert into clients (address, email, firstname, lastname, mobile, notes, status)
values ('12 rue roi Arthur', 'marc.lafoine@gmail.com', 'Marc', 'Lafoine', '023412681', null, true);

insert into products (category, denomination, gear_box, number_of_beds, picture, price_ttc, year_of_manufacture)
values ('van', 'Volkswagen CALIFORNIA Beach', 'Automatique', 4, 'assets/img/produitCc.png', 12000, 2020);
insert into products (category, denomination, gear_box, number_of_beds, picture, price_ttc, year_of_manufacture)
values ('van', 'Volkswagen CALIFORNIA', 'Automatique', 4, 'assets/img/produitCc.png', 13000, 2021);
insert into products (category, denomination, gear_box, number_of_beds, picture, price_ttc, year_of_manufacture)
values ('Capucine', 'Fiat AUTOSTAR auros 15', 'Manuel', 4, 'assets/img/produitCc.png', 8500, 2005);
insert into products (category, denomination, gear_box, number_of_beds, picture, price_ttc, year_of_manufacture)
values ('Fourgon', 'Fiat CHAUSSON', 'Automatique', 1, 'assets/img/produitCc.png', 9800, 2006);
insert into products (category, denomination, gear_box, number_of_beds, picture, price_ttc, year_of_manufacture)
values ('Capucine', 'Fiat MC LOUIS Tandy', 'Automatique', 5, 'assets/img/produitCc.png', 9800, 2006);
insert into products (category, denomination, gear_box, number_of_beds, picture, price_ttc, year_of_manufacture)
values ('Van', 'Volkswagen TRANSPORTEUR OUEST', 'Manuel', 2, 'assets/img/produitCc.png', 10000, 2021);
insert into products (category, denomination, gear_box, number_of_beds, picture, price_ttc, year_of_manufacture)
values ('Van', 'Volkswagen CALIFORNIA TS', 'Manuel', 4, 'assets/img/produitCc.png', 8500, 2005);

insert into orders (order_date, order_state, payment_method, client_id, product_id)
values ('21/04/2022', 'Shipped', 'CB',1, 1);
insert into orders (order_date, order_state, payment_method, client_id, product_id)
values ('12/03/2022', 'Delivered', 'CB',2, 3);