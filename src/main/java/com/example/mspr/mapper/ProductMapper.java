package com.example.mspr.mapper;

import com.example.mspr.api.dto.ProductDto;
import com.example.mspr.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ProductMapper {

    ProductDto mapToDto(Product product);

    Product mapToModel(ProductDto productDto);
}
