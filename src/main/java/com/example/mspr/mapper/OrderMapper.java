package com.example.mspr.mapper;

import com.example.mspr.api.dto.OrderDisplayDto;
import com.example.mspr.api.dto.OrderDto;
import com.example.mspr.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "Spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OrderMapper {

    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "product.id", target = "productId")
    OrderDto mapToDto(Order order);

    @Mapping(source = "product", target = "productDto")
    @Mapping(source = "customer", target = "customerDto")
    OrderDisplayDto mapToDtoDisplay(Order order);

    @Mapping(source = "productId", target = "product.id")
    @Mapping(source = "customerId", target = "customer.id")
    Order mapToModel(OrderDto orderDto);

    @Mapping(source = "productDto", target = "product")
    @Mapping(source = "customerDto", target = "customer")
    Order mapToModelDisplay(OrderDisplayDto orderDisplayDto);
}
