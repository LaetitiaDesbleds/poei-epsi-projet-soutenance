package com.example.mspr.mapper;

import com.example.mspr.api.dto.UserDto;
import com.example.mspr.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UserMapper {

    @Mapping(target="password", ignore=true)
    UserDto mapToUserDto(User user);

    User mapToModel(UserDto UserDto);
}
