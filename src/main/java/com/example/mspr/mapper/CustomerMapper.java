package com.example.mspr.mapper;

import com.example.mspr.api.dto.CustomerDto;
import com.example.mspr.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CustomerMapper {

    CustomerDto mapToCustomerDto(Customer customer);

    Customer mapToModel(CustomerDto customerDto);
}
