package com.example.mspr.service;

import com.example.mspr.entity.Customer;

import java.util.List;

public interface CustomerService {
    Customer getById(Integer id);
    List<Customer> getAll();
    Customer createCustomer(Customer customer);
    Customer updateCustomer(Customer customer);
    void deleteCustomer(Integer id);


}
