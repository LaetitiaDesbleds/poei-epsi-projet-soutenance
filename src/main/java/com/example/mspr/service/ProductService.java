package com.example.mspr.service;

import com.example.mspr.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAll();

    List<Product> getAllWithNoOrder();

    Product getById(Integer id);

    /**
     * Create a product
     * @param product
     * @return
     */
    Product createProduct(Product product);

    void deleteProduct(Integer id);

    Product updateProduct(Product product);
}
