package com.example.mspr.service.serviceImpl;

import com.example.mspr.entity.Customer;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.repository.CustomerRepository;
import com.example.mspr.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer getById(Integer id) {
        return this.customerRepository
                .findById(id)
                .orElseThrow(()-> new UnknownResourceException("No customer found with this id"));
    }

    @Override
    public List<Customer> getAll() {
        return this.customerRepository.findAll();
    }

    @Override
    public Customer createCustomer(Customer customer) {
        customer.setId(null);
        return this.customerRepository.save(customer);
    }

    @Override
    public Customer updateCustomer(Customer customer) {
       Customer existingCustomer = this.getById(customer.getId());
       existingCustomer.setId(customer.getId());
       existingCustomer.setLastname(customer.getLastname());
       existingCustomer.setFirstname(customer.getFirstname());
       existingCustomer.setEmail(customer.getEmail());
       existingCustomer.setAddress(customer.getAddress());
       existingCustomer.setMobile(customer.getMobile());
       existingCustomer.setStatus(false);
       existingCustomer.setNotes(customer.getNotes());
       return this.customerRepository.save(existingCustomer);
    }

    @Override
    public void deleteCustomer(Integer id) {
        Customer customerToDelete = this.getById(id);
        this.customerRepository.delete(customerToDelete);

    }
}
