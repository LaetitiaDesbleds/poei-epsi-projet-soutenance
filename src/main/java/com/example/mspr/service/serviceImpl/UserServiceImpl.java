package com.example.mspr.service.serviceImpl;

import com.example.mspr.entity.User;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.repository.UserRepository;
import com.example.mspr.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User getUserById(Integer id) {
        return this.userRepository
                .findById(id)
                .orElseThrow(() -> new UnknownResourceException("No user found with the given ID"));
    }

    @Override
    public List<User> getAll() {
        return this.userRepository.findAll();
    }

    @Override
    public User getByUsername(String username) {
        return this.userRepository.findByUsername(username)
                .orElseThrow();
    }

    @Override
    public User createUser(User user) {
        String passwordEncoded = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(passwordEncoded);
        user.setGrants("USER");
        return this.userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        User userToDelete = this.getUserById(id);
        this.userRepository.delete(userToDelete);

    }

    @Override
    public User updateUser(User user) {
        User userExistingToUpdate = this.getUserById(user.getId());
        userExistingToUpdate.setGrants(user.getGrants());
        userExistingToUpdate.setUsername(user.getUsername());
        String passwordEncoded = new BCryptPasswordEncoder().encode(user.getPassword());
        userExistingToUpdate.setPassword(passwordEncoded);
        userExistingToUpdate.setFirstname(user.getFirstname());
        userExistingToUpdate.setLastname(user.getLastname());
        userExistingToUpdate.setEmail(user.getEmail());
        userExistingToUpdate.setJob(user.getJob());
        userExistingToUpdate.setEmployeeNumber(user.getEmployeeNumber());
//        userExistingToUpdate.setPassword(user.getPassword());
        return this.userRepository.save(userExistingToUpdate);
    }

    @Override
    public User getUsernameAndPassword(String username, String password) {
        User user = this.userRepository.findByUsername(username).get();
        if (new BCryptPasswordEncoder().matches(password, user.getPassword())) {
            return user;
        }
        throw new UnknownResourceException("No user found for the given username and password");
    }
}
