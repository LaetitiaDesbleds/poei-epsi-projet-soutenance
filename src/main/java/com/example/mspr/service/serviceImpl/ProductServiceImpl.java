package com.example.mspr.service.serviceImpl;

import com.example.mspr.entity.Product;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.repository.ProductRepository;
import com.example.mspr.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return this.productRepository.findAll();
    }

    @Override
    public List<Product> getAllWithNoOrder() {
        return this.productRepository.findAllByOrderIsNull();
    }

    @Override
    public Product getById(Integer id) {
        return productRepository
                .findById(id)
                .orElseThrow(() -> new UnknownResourceException(("No product found for the given ID")));
    }

    @Override
    public Product createProduct(Product product) {
        return this.productRepository.save(product);
    }

    @Override
    public void deleteProduct(Integer id) {
        Product productToDelete = this.getById(id);
        this.productRepository.delete(productToDelete);
    }

    @Override
    public Product updateProduct(Product product) {
        Product existingProduct = this.getById(product.getId());
        existingProduct.setDenomination(product.getDenomination());
        existingProduct.setPrice_ttc(product.getPrice_ttc());
        existingProduct.setGear_box(product.getGear_box());
        existingProduct.setCategory(product.getCategory());
        existingProduct.setNumberOfBeds(product.getNumberOfBeds());
        existingProduct.setYearOfManufacture(product.getYearOfManufacture());
        existingProduct.setPicture(product.getPicture());

        return this.productRepository.save(existingProduct);
    }
}
