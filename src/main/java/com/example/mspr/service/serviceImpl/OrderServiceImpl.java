package com.example.mspr.service.serviceImpl;

import com.example.mspr.entity.Order;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.repository.OrderRepository;
import com.example.mspr.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Order> getAll() {
        return this.orderRepository.findAll();
    }

    @Override
    public Order getById(Integer id) {
        return this.orderRepository.findById(id).orElseThrow(() ->
                new UnknownResourceException("No order found for the given id"));
    }

    @Override
    public Order createOrder(Order order) {
        log.info("Attemptin to create an order");
        return this.orderRepository.save(order);
    }

    @Override
    public void deleteOrder(Integer id) {
        Order orderToDelete = this.getById(id);
        this.orderRepository.delete(orderToDelete);
    }
}
