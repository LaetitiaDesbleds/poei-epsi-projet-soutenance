package com.example.mspr.service;

import com.example.mspr.entity.Order;

import java.util.List;

public interface OrderService {
    List<Order> getAll();

    Order getById(Integer id);

    Order createOrder(Order order);

    void deleteOrder(Integer id);
}
