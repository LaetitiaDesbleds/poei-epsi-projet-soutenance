package com.example.mspr.config;

import com.example.mspr.entity.User;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User existingUser = this.userService.getByUsername(username);
            return new MyUserDetails(existingUser);
        } catch (UnknownResourceException ure) {
            throw new UsernameNotFoundException("User with username" + username + " not found");
        }

    }
}