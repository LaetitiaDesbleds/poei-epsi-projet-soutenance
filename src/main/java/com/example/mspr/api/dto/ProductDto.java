package com.example.mspr.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductDto {

    private Integer id;

    private String denomination;

    private Double price_ttc;

    private String gear_box;

    private String category;

    private Integer numberOfBeds;

    private Integer yearOfManufacture;

    private String picture;

}
