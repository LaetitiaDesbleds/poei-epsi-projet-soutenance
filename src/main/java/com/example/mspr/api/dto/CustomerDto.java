package com.example.mspr.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {

    private Integer id;

    private String lastname;

    private String firstname;

    private String email;

    private String address;

    private String mobile;

    private boolean status;

    private String notes;

}
