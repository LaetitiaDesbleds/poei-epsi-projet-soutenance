package com.example.mspr.api.dto;

import lombok.Data;

@Data
public class UserDto {
    private Integer id;

    private Integer employeeNumber;

    private String username;

    private String lastname;

    private String firstname;

    private String email;

    private String password;

    private String grants;

    private String job;
}
