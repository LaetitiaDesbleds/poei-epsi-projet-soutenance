package com.example.mspr.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
    private Integer id;
    private String orderDate;
    private String orderState;
    private String paymentMethod;
    private Integer customerId;
    private Integer productId;
}
