package com.example.mspr.api.v1;

import com.example.mspr.api.dto.OrderDisplayDto;
import com.example.mspr.api.dto.OrderDto;
import com.example.mspr.exception.NotAllowedToDeleteOrderException;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.mapper.OrderMapper;
import com.example.mspr.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/orders")
public class OrderApi {
    Logger log = LoggerFactory.getLogger(OrderApi.class);
    private final OrderService orderService;
    private final OrderMapper orderMapper;

    public OrderApi(OrderService orderService, OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return the list of all orders")
    public ResponseEntity<List<OrderDisplayDto>> getAll() {
        return ResponseEntity.ok(this.orderService.getAll().stream()
                .map(this.orderMapper::mapToDtoDisplay)
                .toList()
        );
    }


    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return an order")
    public ResponseEntity<OrderDto> getById(@PathVariable final Integer id) {
        try {
            return ResponseEntity.ok(orderMapper.mapToDto(orderService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "Try to create an order", responses = @ApiResponse(responseCode = "201", description = "Created"))
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<OrderDto> createOrder(@RequestBody final OrderDto orderDto) {
        log.debug("Attempting to create order with date {}", orderDto.getOrderDate());
        OrderDto newOrder = orderMapper
                .mapToDto(orderService
                        .createOrder(orderMapper.mapToModel(orderDto)));
        return ResponseEntity.created(URI.create("/v1/orders/" + newOrder.getId())).body(newOrder);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete an order for the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "403", description = "Cannot delete the order for the given ID"),
            @ApiResponse(responseCode = "404", description = "No order found for the given ID")
    })
    public ResponseEntity<Void> deleteOrder(@PathVariable final Integer id) {
        log.debug("Attempting to delete an order with id {}", id);
        try {
            this.orderService.deleteOrder(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        } catch (NotAllowedToDeleteOrderException nde) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, nde.getMessage());
        }
    }

}
