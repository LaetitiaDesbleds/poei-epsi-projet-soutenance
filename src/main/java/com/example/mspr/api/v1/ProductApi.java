package com.example.mspr.api.v1;

import com.example.mspr.api.dto.ProductDto;
import com.example.mspr.exception.NotAllowedToDeleteProductException;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.mapper.ProductMapper;
import com.example.mspr.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/products")
public class ProductApi {

    Logger log = LoggerFactory.getLogger(ProductApi.class);

    private final ProductService productService;
    private final ProductMapper productMapper;

    public ProductApi(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return the list of all products not yet ordered ordered by denomination ascending.")
    public ResponseEntity<List<ProductDto>> getAll() {
        return ResponseEntity.ok(
                this.productService.getAllWithNoOrder().stream()
                .map(this.productMapper::mapToDto)
                .toList()
        );
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "Trying to retrieve a product from the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the product found the given ID"),
            @ApiResponse(responseCode = "404", description = "No product found the given ID")
    })
    public ResponseEntity<ProductDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.productMapper
                    .mapToDto(this.productService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create a product")
    @ApiResponse(responseCode = "201", description = "Created")
    ResponseEntity<ProductDto> createProduct(@RequestBody final ProductDto productDto) {
        log.debug("Attempting to create product with lastname {}", productDto.getDenomination());

        ProductDto productDtoResponse =
                this.productMapper.mapToDto(
                        this.productService.createProduct(
                                this.productMapper.mapToModel(productDto)
                        ));
        return ResponseEntity.created(URI.create("/v1/products" + productDtoResponse.getId()))
                .body(productDtoResponse);
    }


    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a product for given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "403", description = "Cannot delete the product for the given ID"),
            @ApiResponse(responseCode = "404", description = "No product found the given ID")
    })
    public ResponseEntity<Void> deleteProduct(@PathVariable final Integer id) {
        log.debug("Attempting to delete a product with id {}", id);
        try {
            this.productService.deleteProduct(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        } catch (NotAllowedToDeleteProductException nde) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, nde.getMessage());
        }
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "Update a product")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No product found the given ID")
    })
    public ResponseEntity<Void> updateProduct(@PathVariable final Integer id, @RequestBody ProductDto productDto) {
        try {
            log.debug("Updating product {}", productDto.getId());
            productDto.setId(id);
            this.productService.updateProduct(productMapper.mapToModel(productDto));
            log.debug("Successfully updated product {}", productDto.getId());

            return ResponseEntity.noContent().build();
        }catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }


}
