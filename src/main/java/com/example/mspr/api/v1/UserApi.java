package com.example.mspr.api.v1;

import com.example.mspr.api.dto.UserDto;
import com.example.mspr.exception.NotAllowedToDeleteCustomerException;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.mapper.UserMapper;
import com.example.mspr.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
@Data
@AllArgsConstructor
public class UserApi {
    private final UserService userService;

    private final UserMapper userMapper;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return the list of all users")
    public ResponseEntity<List<UserDto>> getAll() {

        return ResponseEntity.ok(
                this.userService.getAll().stream()
                        .map(this.userMapper::mapToUserDto)
                        .toList()
        );

    }

    @GetMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return a user by its id")

    public ResponseEntity<UserDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.userMapper
                    .mapToUserDto(this.userService.getUserById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create an user")
    @ApiResponse(responseCode = "201", description = "Created")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<UserDto> createUser(@RequestBody final UserDto userDto) {
        UserDto userCreated =
                this.userMapper.mapToUserDto(
                        this.userService.createUser(
                                this.userMapper.mapToModel(userDto)
                        )
                );

        return ResponseEntity.created(URI.create("/v1/users/" + userCreated.getId()))
                .body(userCreated);
    }

    @DeleteMapping(path= "/{id}")
    @Operation(summary = "Delete an user" )
    public ResponseEntity<Void> deleteUser(@PathVariable final Integer id){

        try{
            this.userService.deleteUser(id);
            return ResponseEntity.noContent().build();

        } catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());

        } catch(NotAllowedToDeleteCustomerException ex){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());

        }
    }

    @PutMapping(path="/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary= "update an user")
    public ResponseEntity<UserDto> updateUser(@PathVariable final Integer id,
                                              @RequestBody UserDto userDto){
        try{
            userDto.setId(id);

            UserDto updatedUser = userMapper.mapToUserDto(userService
                    .updateUser(userMapper
                            .mapToModel(userDto)));
            return ResponseEntity.ok(updatedUser);
        }catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }

    }

    @GetMapping(path = "/login", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary="Return a user by its username and password")
    public ResponseEntity<UserDto> login(@RequestParam String username,
                                         @RequestParam String password){
        try{
            return ResponseEntity.ok(
                    userMapper.mapToUserDto(userService.getUsernameAndPassword(username, password))
            );
        }catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }

    }
}
