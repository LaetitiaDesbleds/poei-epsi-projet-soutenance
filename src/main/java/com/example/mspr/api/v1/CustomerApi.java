package com.example.mspr.api.v1;

import com.example.mspr.api.dto.CustomerDto;
import com.example.mspr.exception.NotAllowedToDeleteCustomerException;
import com.example.mspr.exception.UnknownResourceException;
import com.example.mspr.mapper.CustomerMapper;
import com.example.mspr.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/customers")
@Data
public class CustomerApi {

    private final CustomerService customerService;

    private final CustomerMapper customerMapper;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(
            summary = "Return the list of all customers "
    )
    public ResponseEntity<List<CustomerDto>> getAll() {
        return ResponseEntity.ok(
                this.customerService.getAll().stream()
                        .map(this.customerMapper::mapToCustomerDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "Trying to retrieve a customer from the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the customer found the given ID"),
            @ApiResponse(responseCode = "404", description = "No customer found the given ID")
    })
    public ResponseEntity<CustomerDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.customerMapper
                    .mapToCustomerDto(this.customerService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }


    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create a customer")
    @ApiResponse(responseCode = "201", description = "Created")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<CustomerDto> createCustomer(@RequestBody final CustomerDto customerDto) {

        CustomerDto customerDtoResponse =
                this.customerMapper.mapToCustomerDto(
                        this.customerService.createCustomer(
                                this.customerMapper.mapToModel(customerDto)

                        )
                );

        return ResponseEntity.created(URI.create("/v1/customers/" + customerDtoResponse.getId()))
                .body(customerDtoResponse);
    }

    @DeleteMapping(path= "/{id}")
    @Operation(summary = "Delete a customer for the given ID" )
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "403", description = "Cannot delete the customers for the given ID"),
            @ApiResponse(responseCode = "404", description = "No customer found the given ID")
    })

    public ResponseEntity<Void> deleteCustomer(@PathVariable final Integer id){

        try{
            this.customerService.deleteCustomer(id);
            return ResponseEntity.noContent().build();

        } catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());

        } catch(NotAllowedToDeleteCustomerException ex){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());

        }
    }

    @PutMapping(path="/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary= "update a customer")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "no content")
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> updateCustomer(@PathVariable final Integer id,
                                              @RequestBody CustomerDto customerDto){
        try{
            customerDto.setId(id);
            this.customerService.updateCustomer(customerMapper.mapToModel(customerDto));
            return ResponseEntity.noContent().build();
        }catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }

    }
}
