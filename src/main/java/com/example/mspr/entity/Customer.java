package com.example.mspr.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Table(name="clients")
@Entity
@Data
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column( length = 100)
    private String lastname;

    @Column(length = 100)
    private String firstname;

    @Column(nullable = false)
    private String email;

    @Column( length = 255)
    private String address;

    @Column( length = 15)
    private String mobile;

    private boolean status;

    @Column( columnDefinition = "TEXT")
    private String notes;

    @OneToMany(mappedBy = "customer")
    private Set<Order> orders;

}
