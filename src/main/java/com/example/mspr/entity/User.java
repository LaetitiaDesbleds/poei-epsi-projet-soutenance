package com.example.mspr.entity;

import lombok.Data;

import javax.persistence.*;

@Table(name="users")
@Entity
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column( length = 10)
    private Integer employeeNumber;

    @Column( length = 100)
    private String username;

    @Column( length = 100)
    private String lastname;

    @Column( length = 100)
    private String firstname;

    private String email;

    private String password;

    private String grants;

    private String job;


}
