package com.example.mspr.entity;


import lombok.Data;

import javax.persistence.*;

@Table(name="products")
@Entity
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(nullable = false)
    private String denomination;

    @Column(nullable = false)
    private Double price_ttc;

    @Column(nullable = false)
    private String gear_box;

    @Column(nullable = false)
    private String category;

    @Column(nullable = false)
    private Integer numberOfBeds;

    @Column(nullable = false)
    private Integer yearOfManufacture;

    @Column(nullable = false)
    private String picture;

    @OneToOne(mappedBy ="product")
    private Order order;




}
