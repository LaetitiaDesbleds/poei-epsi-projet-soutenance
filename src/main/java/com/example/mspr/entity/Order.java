package com.example.mspr.entity;

import lombok.Data;

import javax.persistence.*;


@Table(name="orders")
@Entity
@Data
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(nullable = false)
    private String orderDate;

    @Column(nullable = false, length = 100)
    private String orderState;

    @Column(nullable = false, length = 100)
    private String paymentMethod;


    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="client_id", nullable = false)
    private Customer customer;

    @OneToOne
    private Product product;

}

