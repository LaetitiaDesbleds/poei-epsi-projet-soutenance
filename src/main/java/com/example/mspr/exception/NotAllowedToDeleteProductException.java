package com.example.mspr.exception;

public class NotAllowedToDeleteProductException extends RuntimeException {

    public NotAllowedToDeleteProductException(String message) {
        super(message);
    }

    public NotAllowedToDeleteProductException() {
        super("Cannot delete the given product");
    }
}
